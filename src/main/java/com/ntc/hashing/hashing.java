package com.ntc.hashing;

import java.util.Scanner;

/**
 *
 * @author L4ZY
 */
public class hashing<Key, Value> {

    int hashSize = 30001;
    Value[] vals = (Value[]) new Object[hashSize];
    Key[] keys = (Key[]) new Object[hashSize];

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % hashSize;
    }

    public void Put(Key key, Value value) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % hashSize) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        vals[i] = value;
    }

    public Value Get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % hashSize) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
        }
        return null;
    }

    public static void main(String[] args) {
        hashing<String, String> hashing = new hashing<>();
        Scanner kb = new Scanner(System.in);
        String key = kb.next();
        String value = kb.next();
        hashing.Put(key, value);
        String Finding = kb.next();
        if (hashing.Get(Finding) == null) {
            System.out.println("not found");
        } else {
            System.out.println(hashing.Get(Finding));
        }
    }

}
